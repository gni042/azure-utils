#!/bin/bash

# This script mounts a drive that has
#  - ONLY ONE PARTITION
#  - IS STILL UNMOUNTED (does not have a mountpoint)
#
# USAGE: mount_disk_by_lun.sh <LUN> <MOUNTPOING>
#   <LUN>: Logical Unit Number, specified in Azure when attaching the disk to
#       the VM.
#   <MOUNTPOINT>: Mount point for the drive. It will be created if it did not
#       exist
#
# ERROR CODES: The script with the following error codes:
#     (1) Illegal number of arguments
#     (2) No drives are assigned to the provided LUN
#     (3) There are more than one drives on the provided LUN
#     (4) The LUN-assigned drive is not partitioned
#     (5) There are more than one partitions
#     (6) The partition is already mounted
#     (7) The drive is not formatted as "ext2", "ext3", or "ext4"


if [[ $# -ne 2 ]]; then
    echo "[ERROR] Provide LUN (HOST:CHANNEL:TARGE:LUN) and the mountpoint destination"
    echo "USAGE: mount_disk_by_lun.sh <LUN> <MOUNTPOINT>"
    exit 1
fi

LUN=$1
DESTINATION=$2
DEV=( $( lsblk -o NAME,HCTL,TYPE | grep "^sd" | awk -v lun=$LUN 'BEGIN{regexp="^[0-9]+:[0-9]+:[0-9]+:"lun"$"}{if ($2 ~ regexp && $3 ~ /disk/) { print $1 }}' ) )

if [[ -z "$DEV" ]]; then
    echo "[ERROR] No device found with LUN=$LUN"
    exit 2
fi

if [[ "${#DEV[@]}" -ne 1 ]]; then
    echo "[ERROR] More than one disk on LUN=$LUN:"
    printf '/dev/%s ' "${DEV[@]}" | xargs lsblk -o NAME,HCTL,SIZE,TYPE
    exit 3
fi


DEV=${DEV[0]}

# Find out whether the disk has partitions
PART=( $( lsblk -o NAME,TYPE,MOUNTPOINT /dev/$DEV | awk '$2 ~ "part"{ print $1 }' ) )
MNTPT=( $( lsblk -o NAME,TYPE,MOUNTPOINT /dev/$DEV | awk '$2 ~ "part"{ print $3 }' ) )
FS=( $( lsblk -o NAME,TYPE,FSTYPE /dev/$DEV | awk '$2 ~ "part"{ print $3 }' ) )
PTNAME=( $( lsblk -o KNAME,TYPE,FSTYPE /dev/$DEV | awk '$2 ~ "part"{ print $1 }' ) )

if [[ "${#PART[@]}" -eq 0 ]]; then
    echo "[ERROR] Disk /dev/$DEV is not partitioned (ONE partition required)"
    lsblk -o NAME,HCTL,SIZE,TYPE,MOUNTPOINT,FSTYPE /dev/$DEV
    exit 4
fi

if [[ "${#PART[@]}" -gt 1 ]]; then
    echo "[ERROR] Disk /dev/$DEV has more than one partition (${#PART[@]}). Only ONE partition allowed"
    lsblk -o NAME,TYPE,MOUNTPOINT,FSTYPE /dev/$DEV
    exit 5
fi

if [[ "${#MNTPT[@]}" -gt 1 ]]; then
    echo "[ERROR] UNEXPECTED ERROR: one partition, several mount points (???)"
    exit 98
fi

if [[ "${#FS[@]}" -gt 1 ]]; then
    echo "[ERROR] UNEXPECTED ERROR: one partition, one (or none) mount point, several File systems (???)"
    exit 99
fi

MNTPT=${MNTPT[0]}
FS=${FS[0]}
PTNAME=${PTNAME[0]}

if [[ ! -z "${MNTPT}" ]]; then
    echo "[ERROR] The partition is already mounted"
    lsblk -o NAME,HCTL,SIZE,TYPE,MOUNTPOINT,FSTYPE /dev/${PTNAME}
    exit 6
fi
    
if [[ ${FS} == "ext2" || ${FS} == "ext3" || ${FS} == "ext4" ]]; then
    echo "LUN=${LUN}: one partition in ext fileformat without mountpoint:"
    lsblk -o NAME,KNAME,HCTL,SIZE,TYPE,FSTYPE /dev/${DEV}
    sudo mkdir -p ${DESTINATION}
    sudo mount /dev/${PTNAME} ${DESTINATION}
    if [[ $? == 0 ]]; then
        echo "MOUNTED IN ${DESTINATION}"
    else
        echo "[ERROR] Could not mount in ${DESTINATION}"
    fi
else
    echo "[ERROR] The partition is not in ext2, ext3 or ext4 format"
    exit 7
fi




