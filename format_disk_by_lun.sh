#!/bin/bash

# This script partitions and formats a drive that has
#  - NO PARTITIONS
#
# USAGE: format_disk_by_lun.sh <LUN>
#   <LUN>: Logical Unit Number, specified in Azure when attaching the disk to
#       the VM.
#
# ERROR CODES: The script with the following error codes:
#     (1) Illegal number of arguments
#     (2) No drives are assigned to the provided LUN
#     (3) There are more than one drives on the provided LUN
#     (4) The LUN-assigned drive is already partitioned


if [[ $# -ne 1 ]]; then
    echo "[ERROR] Provide LUN (HOST:CHANNEL:TARGE:LUN)"
    echo "USAGE: mount_disk_by_lun.sh <LUN>"
    exit 1
fi

LUN=$1
DEV=( $( lsblk -o NAME,HCTL,TYPE | grep "^sd" | awk -v lun=$LUN 'BEGIN{regexp="^[0-9]+:[0-9]+:[0-9]+:"lun"$"}{if ($2 ~ regexp && $3 ~ /disk/) { print $1 }}' ) )

if [[ -z "$DEV" ]]; then
    echo "[ERROR] No device found with LUN=$LUN"
    exit 2
fi

if [[ "${#DEV[@]}" -ne 1 ]]; then
    echo "[ERROR] More than one disk on LUN=$LUN:"
    printf '/dev/%s ' "${DEV[@]}" | xargs lsblk -o NAME,HCTL,SIZE,TYPE
    exit 3
fi


DEV=${DEV[0]}

# Find out whether the disk has partitions
PART=( $( lsblk -o NAME,TYPE,MOUNTPOINT /dev/$DEV | awk '$2 ~ "part"{ print $1 }' ) )

if [[ "${#PART[@]}" -gt 0 ]]; then
    echo "[ERROR] Disk /dev/$DEV is already partitioned"
    lsblk -o NAME,HCTL,SIZE,TYPE,MOUNTPOINT,FSTYPE /dev/$DEV
    exit 4
fi

echo "PARTITIONING /dev/$DEV..."

sudo parted /dev/$DEV --script mklabel gpt mkpart ext4 0% 100%

echo "FORMATTING /dev/${DEV}1 to ext4..."

sleep 10

sudo mkfs.ext4 /dev/${DEV}1



