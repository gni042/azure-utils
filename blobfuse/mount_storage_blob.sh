#!/bin/bash

resourceGroupName="FOR-NEURO-SYSMED-SHARED-STORAGE"
storageAccountName="forneurosysmedimport"
containerName="tsd-import1"
user="gsnido"

ramdisk="/mnt/blobfusetmp"
mountpoint="/home/${user}/tsd-import"

# Login to azure
az login
az account set --subscription FOR-NEURO-SYSMED-SHARED

# This command assumes you have logged in with az login
httpEndpoint=$(az storage account show \
    --resource-group $resourceGroupName
        --name $storageAccountName
            --query "primaryEndpoints.file" | tr -d '"')
smbPath=$(echo $httpEndpoint | cut -c7-$(expr length $httpEndpoint))
fileHost=$(echo $smbPath | tr -d "/")

# Test connection
echo "Testing connection..."
nc -zvw3 $fileHost 445

if [ ! -d ${ramdisk} ]; then
    exit "Ramdisk needs to exit and be mounted"
fi

if [ ! -d $mountpoint ]; then
    mkdir -p $mountpoint
fi

# Mount via blobfuse
echo "Mounting..."
sudo blobfuse $mountpoint --tmp-path=${ramdisk} \
    --config-file=/home/$user/fuse_connection.cfg -o attr_timeout=240 \
    -o entry_timeout=240 -o negative_timeout=120 -o umask=137 -o allow_other \
    -o uid=1000 -o gid=1000
    
# UNMOUNT:
# fusermount -u $mountpoint

