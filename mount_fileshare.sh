#!/bin/bash

set -e 

#### FUNCTIONS
usage()
{
    echo ""
    echo "USAGE:"
    echo "$0 [OPTIONS] -s STORAGEACCTNAME -f FILESHARENAME"
    echo ""
    echo "OPTIONS"
    echo "   -s STORAGEACCTNAME"
    echo "          Storage account name [REQUIRED]."
    echo "   -f FILESHARENAME"
    echo "          Name of fileshare to mount [REQUIRED]."
    echo "   -m MOUNTPOINT"
    echo "          Directory where file share is to be mounted."
    echo "   -p"
    echo "          Persist mount adding it to /etc/fstab [NOT IMPLEMENTED]."
    echo "   -c"
    echo "          Samba credentials file (defaults to /etc/smbcredentials/STORAGEACCTNAME.cred)."
    echo "   -u INTEGER"
    echo "          UID for user that will own mount (default: 1000)."
    echo "   -g INTEGER"
    echo "          GID for group that will own mount (default: 1000)."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

###### PARSE CMDLINE

STORAGEACCT=
SHARENAME=
MOUNTPOINT=
PERSIST=0
SMBCRED=
USERID=1000
GROUPID=1000

while getopts ":s:f:m:pc:u:g:h" opt; do
    case ${opt} in 
        s)
            STORAGEACCT=$OPTARG
            ;;
        f)
            SHARENAME=$OPTARG
            ;;
        m)
            MOUNTPOINT=$OPTARG
            ;;
        p)
            PERSIST=1
            ;;
        c)
            SMBCRED=$OPTARG
            ;;
        u)
            USERID=$OPTARG
            ;;
        g)
            GROUPID=$OPTARG
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

if [[ ${PERSIST} == 1 ]]; then
    echo "Option -p is still not implemented, sorry."
fi

if [[ $STORAGEACCT == "" ]]; then
    echo "[ERROR] Storage account name (-s option) is required."
    exit 1
fi
if [[ $SHARENAME == "" ]]; then
    echo "[ERROR] File share name (-f option) is required."
    exit 1
fi

if [[ $SMBCRED == "" ]]; then
    SMBCRED=/etc/smbcredentials/${STORAGEACCT}.cred
fi

if [[ $MOUNTPOINT == "" ]]; then
    MOUNTPOINT=/fileshares/${STORAGEACCT}/${SHARENAME}
fi

sudo mkdir -p ${MOUNTPOINT}

sudo mount -t cifs //${STORAGEACCT}.file.core.windows.net/${SHARENAME} \
    ${MOUNTPOINT} \
    -o vers=3.0,credentials=${SMBCRED},dir_mode=0755,file_mode=0644,serverino,gid=${GROUPID},uid=${USERID},forceuid,forcegid

if [[ $? == 0 ]]; then
    echo "File share mounted in ${MOUNTPOINT}"
fi

exit 0

