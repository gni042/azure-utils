#!/bin/bash

echo "#### Updating Ubuntu dist..."
sudo apt update && sudo apt -y upgrade

echo "#### Adding microsoft repo..."
curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
sudo apt-add-repository https://packages.microsoft.com/ubuntu/18.04/prod
sudo apt update


echo "#### Installing some Ubuntu packages and libs..."
sudo apt -y install unzip build-essential libncurses5-dev zlib1g-dev libbz2-dev liblzma-dev p7zip-full pigz neovim
sudo apt -y install libcurl4-gnutls-dev libgnutls28-dev

echo "#### Installing configs..."
cd ~
git clone https://git.app.uib.no/gni042/configs.git
cd configs
bash INSTALL.sh
cd ~


echo "Installing R..."
sudo apt -y install r-base
sudo apt -y install libcurl4-openssl-dev libxml2-dev

echo "#### Installing blobfuse and cifs for mounting..."
#wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb
#sudo dpkg -i packages-microsoft-prod.deb
sudo apt -y install blobfuse fuse cifs-utils autofs

